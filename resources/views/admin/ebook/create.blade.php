@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
				<div class="row">
					<div class="col-xl-9 mx-auto">
						<h6 class="mb-0 text-uppercase">Tambah Ebook</h6>
						<hr/>
						<div class="card">
							<div class="card-body">
								<div class="p-4 border rounded">
									<form class="row g-3 needs-validation"action="/ebook/store" method="POST" enctype="multipart/form-data" novalidate>@csrf
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Id Kategori</label>
											<input type="text" class="form-control"name="category_id" id="nama_category" placeholder="Masukkan Id Kategori" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Id User</label>
										

<select class="form-control" name="payment_status" id="combo1">
	<option value="">Pilih</option>
		 @forelse ($user as $key=>$value)
	<option value="{{$value->id}}">{{$value->id}}</option>
	@empty
		<option value="No Data">No Data</option>
@endforelse

</select>
											
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Name</label>
											<input type="text" class="form-control"name="name" id="nama_category" placeholder="Masukkan Nama Ebook" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
												<div class="row">
					<div class="col-xl-12 mx-auto">
						<h6 class="mb-0 text-uppercase">Upload File</h6>
						<hr/>
						<div class="card">
							<div class="card-body">
								<form>
									<input id="image-uploadify" type="file" name="file_pdf" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>
								</form>
							</div>
						</div>
					</div>
									</div>
				<!--end row-->
				<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Harga</label>
											<input type="text" class="form-control"name="harga" id="nama_category" placeholder="Masukkan Harga" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Deskripsi</label>
											<input type="text" class="form-control"name="deskripsi" id="nama_category" placeholder="Masukkan Deskripsi" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-12">
											<button class="btn btn-light" type="submit">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
				<!--end row-->
				<br>
				<br>
					<br>
				<br>

			</div>

	<div class="overlay toggle-icon"></div>
		<!--end overlay-->
		<!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
		<!--End Back To Top Button-->


@endsection