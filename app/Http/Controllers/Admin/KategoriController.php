<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Category;
use pdf;
//use UxWeb\SweetAlert\SweetAlert;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $kategori = Category::all();
        return view('admin.kategori.index', compact('kategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('admin.kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
        $this->validate($request,[
            'nama_category' => 'required'
       
        ]);
 
        Kategori::create([
            'nama_category' => $request->nama_category
        
        ]);
      //   SweetAlert::success('Success Message','Data berhasil disimpan');
        return redirect('/kategori');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          $kategori = Kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $kategori = Category::find($id);
        return view('admin.kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'nama_category' => 'required',
          
        ]);

        $kategori = Category::find($id);
        $kategori->nama_category = $request->nama_category;
        $kategori->update();
              //  SweetAlert::success('Success Message','Data berhasil diedit');
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
            $kategori = Category::find($id);
        $kategori->delete();
             //   SweetAlert::success('Success Message','Data berhasil dihapus');
        return redirect('/kategori');
    }
        public function cetak_pdf()
    {
        $kategori = Kategori::all();
 
        $pdf = PDF::loadview('pegawai_pdf',['kategori'=>$kategori]);
        return $pdf->download('laporan-pegawai-pdf');
    }
}
