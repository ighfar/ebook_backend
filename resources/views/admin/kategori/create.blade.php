@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
				<div class="row">
					<div class="col-xl-9 mx-auto">
						
						<h6 class="mb-0 text-uppercase">Tambah Kategori</h6>
						<hr/>
						<div class="card">
							<div class="card-body">
								<div class="p-4 border rounded">
									<form class="row g-3 needs-validation"action="/kategori/store" method="POST" novalidate>
										<div class="col-md-12">@csrf
											<label for="validationCustom01" class="form-label">Kategori</label>
											<input type="text" class="form-control"name="nama_category" id="nama_category" placeholder="Masukkan Nama Kategori" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-12">
											<button class="btn btn-light" type="submit">Submit form</button>
										</div>
									</form>
								</div>
							</div>
						</div>
				<!--end row-->
				<br>
				<br>
					<br>
				<br>

			</div>


@endsection