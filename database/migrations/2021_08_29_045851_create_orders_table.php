<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create(
            'orders',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedInteger('user_id')->nullable();
                $table->string('code')->unique();
                $table->string('status');
                $table->datetime('order_date');
                $table->datetime('payment_due');
                $table->string('payment_status');
                $table->decimal('base_total_price', 16, 2)->default(0);
                $table->decimal('discount_amount', 16, 2)->default(0);
                $table->decimal('discount_percent', 16, 2)->default(0);
                $table->decimal('grand_total', 16, 2)->default(0);
                $table->text('note')->nullable();
                 $table->string('customer_first_name')->nullable();
                 $table->string('customer_phone')->nullable();
                $table->string('customer_email')->nullable();
                $table->unsignedBigInteger('approved_by')->nullable();
                $table->datetime('approved_at')->nullable();
              
                $table->softDeletes();
                $table->timestamps();
                $table->index('code');
                $table->index(['code', 'order_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
