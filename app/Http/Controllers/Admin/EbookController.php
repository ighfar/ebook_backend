<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Ebook;
use App\User;
use pdf;
use Illuminate\Support\Facades\Validator;
//use UxWeb\SweetAlert\SweetAlert;
use Maatwebsite\Excel\Facades\Excel;

class EbookController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ebook = Ebook::all();
        return view('admin.ebook.index', compact('ebook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
             $user = User::all();
        return view('admin.ebook.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  
       $validator = Validator::make($request->all(), [
            'category_id' => 'required | numeric',
               'user_id'       => 'required',
            'name'       => 'required',
            'file_pdf' => 'required|file|mimes:pdf|max:2048',
            'harga' => 'required | numeric',
            'deskripsi'       => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $file = $request->file('file_pdf')->getClientOriginalName();
        $request->file('file_pdf')->move('upload',$file);
        $data = [
            'category_id' => $request->input('category_id'),
                        'user_id' => $request->input('user_id'),
            'name' => $request->input('name'),
            'file_pdf' => $file,
            'harga' => $request->input('harga'),
            'deskripsi' => $request->input('deskripsi'),
        ];
        $ebook = Ebook::create($data);
               // SweetAlert::success('Success Message','Data berhasil disimpan');
 
        return redirect('/ebook');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $ebook = Ebook::find($id);
        return view('admin.ebook.show', compact('ebook'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
           $ebook = Ebook::find($id);
        return view('admin.ebook.edit', compact('ebook'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
            'category_id' => 'required | numeric',
               'user_id'       => 'required',
            'name'       => 'required',
            'file_pdf' => 'required|file|mimes:pdf|max:2048',
            'harga' => 'required | numeric',
            'deskripsi'       => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $file = $request->file('file_pdf')->getClientOriginalName();
        $request->file('file_pdf')->move('upload',$file);

        $ebook = Ebook::find($id);
        $ebook->category_id = $request->category_id;
              $ebook->user_id = $request->user_id;
        $ebook->name = $request->name;
        $ebook->file_pdf = $request->file_pdf;
        $ebook->harga = $request->harga;
        $ebook->deskripsi = $request->deskripsi;
        $ebook->update();
              //  SweetAlert::success('Success Message','Data berhasil diedit');
        return redirect('/ebook');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {      
        $ebook = Ebook::find($id);
        $ebook->delete();
              //  SweetAlert::success('Success Message','Data berhasil dihapus');
        return redirect('/ebook');
    }
       
}
