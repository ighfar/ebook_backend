<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('category', 'CategoryController@index');
Route::get('ebook/category/{category_id}', 'EbookController@ebookbycategory');
Route::get('ebook', 'EbookController@index');
Route::get('ebook/{id}', 'EbookController@detail_ebook');

Route::get('/carts', 'CartController@index');
Route::delete('/cart_destroy/{id}', 'CartController@destroy');
Route::post('/payment/notification/post','PaymentController@post');


Route::middleware('auth:api')->group( function () {
    Route::get('profil', 'UserController@profil');
    Route::put('update_profil/{id}', 'UserController@update');
    Route::put('change_password', 'UserController@change_password');
    Route::post('add_ebook', 'EbookController@create');
    Route::put('update_ebook/{id}', 'EbookController@update');
    Route::delete('delete_ebook/{id}', 'EbookController@delete');
    Route::post('cart_add/{id}', 'CartController@store');
    Route::post('order/do_checkout/{id}', 'OrderController@docheckout');
    Route::get('orders', 'OrderController@index');
Route::get('orders/{id}', 'OrderController@show');
Route::get('logout', 'UserController@logout');
Route::get('my_ebook', 'userController@ebookku');

Route::post('payments/notification', 'PaymentController@notification');
Route::get('payments/completed', 'PaymentController@completed');
Route::get('payments/failed', 'PaymentController@failed');
Route::get('payments/unfinish', 'PaymentController@unfinish');
});
