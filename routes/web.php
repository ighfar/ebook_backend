<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('kategori', 'Admin\KategoriController@index')->name('kategori');
Route::get('kategori/create', 'Admin\KategoriController@create');
Route::post('kategori/store', 'Admin\KategoriController@store');
Route::get('kategori/{id}/edit', 'Admin\KategoriController@edit');
Route::get('kategori/{id}/delete', 'Admin\KategoriController@delete');
Route::put('kategori/{id}/update', 'Admin\KategoriController@update');
Route::get('ebook', 'Admin\EbookController@index')->name('ebook');
Route::get('ebook/create', 'Admin\EbookController@create');
Route::post('ebook/store', 'Admin\EbookController@store');
Route::get('ebook/{id}/edit', 'Admin\EbookController@edit');
Route::get('ebook/{id}/show', 'Admin\EbookController@show');
Route::get('ebook/{id}/delete', 'Admin\EbookController@destroy');
Route::put('ebook/{id}/update', 'Admin\EbookController@update');
Route::get('transaksi', 'Admin\TransaksiController@index')->name('transaksi');
Route::get('transaksi/{id}/edit', 'Admin\TransaksiController@edit');
Route::put('transaksi/{id}/update', 'Admin\TransaksiController@update');
Route::get('transaksi/{id}/delete', 'Admin\TransaksiController@destroy');
Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile', 'ProfileController@update')->name('profile.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
