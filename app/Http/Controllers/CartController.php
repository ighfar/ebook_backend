<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Ebook;
use Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{

    /**
     * Store a newly created Cart in storage and return the data to the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
   
     return Cart::all();   
    }

    /**
     * Display the specified Cart.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
      
      $userid = Auth::user()->id;
       $ebook = Ebook::where('id',$id)->first();

      
        $data=array();
        $data['ebook_id']=$id;
        $data['user_id']=$userid;
        $data['ebook_name']=$ebook->name;
        $data['ebook_harga']=$ebook->harga;
        $ebook = Cart::create($data);
       return response()->json('Ebook berhasil ditambahkan ke cart ');

    }

    /**
     * Remove the specified Cart from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
public function destroy($id)
    {
          $cart = Cart::find($id);
            $cart->delete();
        return response()->json([
             'message' => 'Ebook telah dihapus dari Cart.',
            ], 200);
    }

    /**
     * Adds Products to the given Cart;
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return void
     */
    /**
     * checkout the cart Items and create and order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return void
     */


}
