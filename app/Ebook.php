<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ebook extends Model
{
    protected $table = 'ebooks';
    protected $fillable = [
        'category_id',
        'user_id',
        'name',
        'file_pdf',
        'harga',
        'deskripsi',
     
    ];
}
