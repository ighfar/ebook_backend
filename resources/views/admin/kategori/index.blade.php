@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
						<h6 class="mb-0 text-uppercase">Kategori</h6>
				<hr/>
					<div class="col-12">
				<a href="kategori/create" class="btn btn-primary">Tambah</a>
			</div>
			<br>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered" style="width:100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Kategori</th>
										<th>Actions</th>
										
									</tr>
								</thead>
						       <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama_category}}</td>
                       
                        <td>
                         
                            <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                           
                             <a href="/kategori/{{$value->id}}/delete" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
							<tfoot>									<tr>
										<th>#</th>
										<th>Kategori</th>
										<th>Actions</th>
										
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>

			</div>


@endsection