<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\User;
use App\Cart;
use App\Payment;
use Auth;

use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Order::all();
    } 
        public function detail_order($id)
    {
        $order = Order::whereId($id)->first();


        if ($order) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Order!',
                'data'    => $order
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Order Tidak Ditemukan!',
                'data'    => ''
            ], 401);
        }
    }


       public function docheckout(Request $request, $id)
    {
                $params = $request->except('_token');

        $order = \DB::transaction(
            function () use ($params) {
                $order = $this->_saveOrder($params);
    
                $this->_generatePaymentToken($order);

    
                return $order;
            }
        );

        if ($order) {
            \Cart::clear();
         
         return $order;
        }

    

       
}   private function _saveOrder($id)
    {
          $user_id = Auth::user()->id;
          $user_name = Auth::user()->name;
         $no_hp = Auth::user()->no_hp;
        $email = Auth::user()->email;
         $cart = Cart::where('id',$id)->first();
        $baseTotalPrice = $cart->ebook_harga;
     
        $discountAmount = 0;
        $discountPercent = 0;
        $grandTotal = $baseTotalPrice - $discountAmount;

        $orderDate = date('Y-m-d H:i:s');
       $paymentDue = (new \DateTime($orderDate))->modify('+1 day')->format('Y-m-d H:i:s');

        $orderParams = [
           'user_id' => $user_id,
            'code' => Order::getAutoNumberOptions(),
            'status' => Order::CREATED,
            'order_date' => $orderDate,
            'payment_due' => $paymentDue,
            'payment_status' => Order::UNPAID,
            'base_total_price' => $baseTotalPrice,
 
            'discount_amount' => $discountAmount,
            'discount_percent' => $discountPercent,
        
            'grand_total' => $grandTotal,
            'customer_first_name' =>$user_name,
  
            'customer_phone' => $no_hp,
            'customer_email' => $email,
            
 
           
        ];
        return Order::create($orderParams);
    }

private function _generatePaymentToken($order)
    {
    // Set your Merchant Server Key
        \Midtrans\Config::$serverKey = 'SB-Mid-server-sLQClvcAuWcUQtIEVJjBXeK5';
        // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
        \Midtrans\Config::$isProduction = false;
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;
        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;



        $customerDetails = [
        'first_name' => $order->customer_first_name,
            'email' => $order->customer_email,
            'phone' => $order->customer_phone,
        ];

        $params = array(

            'transaction_details' => [
                'order_id' => $order->code,
                 'gross_amount' => $order->grand_total
                
            ],
            'customer_details' => $customerDetails,
            'expiry' => [
                'start_time' => date('Y-m-d H:i:s T'),
                'unit' => Payment::EXPIRY_UNIT,
                'duration' => Payment::EXPIRY_DURATION,
            ],
                
        );

        $snap = \Midtrans\Snap::createTransaction($params);
        
        if ($snap->token) {
            $order->payment_token = $snap->token;
            $order->payment_url = $snap->redirect_url;
      
            $order->save();
        }
    }
}
