<?php

namespace App\Http\Resources;
use App\Ebook;


use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
     
       $ebook = Ebook::find($this->ebook_id);
     
        return [
            'ebookID' => $this->ebook_id,
            'SKU' => $ebook->sku,
            'price' => $ebook->harga,
            'Name' => $ebook->name,
         //   'Quantity' => $this->quantity,
        ];
    }
}
