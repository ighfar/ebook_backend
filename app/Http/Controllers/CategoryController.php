<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Ebook;

class CategoryController extends Controller
{
    public function index() {
        return Category::all();
    }

 

     

    public function product_per_category($category_id)
    {
        $product = Product::whereId($category_id)->first();


        if ($product) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Product!',
                'data'    => $product
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Product Tidak Ditemukan!',
                'data'    => ''
            ], 401);
        }
    }
}