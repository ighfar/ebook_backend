@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
				<div class="row">
					<div class="col-xl-9 mx-auto">
						<h6 class="mb-0 text-uppercase">Update Ebook {{$ebook->id}}</h6>
						<hr/>
						<div class="card">
							<div class="card-body">
								<div class="p-4 border rounded">
									<form class="row g-3 needs-validation"action="/ebook/{{$ebook->id}}/update" method="POST" enctype="multipart/form-data" novalidate>
							
													<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Id Kategori</label>
											<input type="text" class="form-control"name="category_id" value="{{$ebook->category_id}}" id="nama_category" placeholder="Masukkan Id Kategori" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Id User</label>
											<input type="text" class="form-control"name="user_id" value="{{$ebook->user_id}}" id="nama_category" placeholder="Masukkan Id User" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Name</label>
											<input type="text" class="form-control"name="name" value="{{$ebook->name}}" id="nama_category" placeholder="Masukkan Nama Ebook" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Upload File Ebook</label>
											<input type="file" class="form-control"name="file_pdf" id="nama_category" placeholder="Upload File" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Harga</label>
											<input type="text" class="form-control"name="harga" value="{{$ebook->harga}}" id="nama_category" placeholder="Masukkan Harga" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Deskripsi</label>
											<input type="text" class="form-control"name="deskripsi" value="{{$ebook->deskripsi}}" id="nama_category" placeholder="Masukkan Deskripsi" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-12">
											<button class="btn btn-light" type="submit">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
				<!--end row-->
				<br>
				<br>
					<br>
				<br>

			</div>


@endsection