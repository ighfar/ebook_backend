@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
				<div class="row">
					<div class="col-xl-9 mx-auto">
						<h6 class="mb-0 text-uppercase">Update Transaksi {{$order->id}}</h6>
						<hr/>
						<div class="card">
							<div class="card-body">
								<div class="p-4 border rounded">
									<form class="row g-3 needs-validation"action="/transaksi/{{$order->id}}/update" method="POST" enctype="multipart/form-data" novalidate>@csrf
											 @method('PUT')
							
													<div class="col-md-12">
											<label for="validationCustom01" class="form-label">User Id</label>
											<input type="text" class="form-control"name="user_id" value="{{$order->user_id}}" id="nama_category" placeholder="Masukkan Id Kategori" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Code</label>
											<input type="text" class="form-control"name="code" value="{{$order->code}}" id="nama_category" placeholder="Masukkan Id User" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Status</label>
											<input type="text" class="form-control"name="status" value="{{$order->status}}" id="nama_category" placeholder="Masukkan Nama Ebook" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">total</label>
											<input type="text" class="form-control"name="grand_total" value="{{$order->grand_total}}" id="nama_category" placeholder="total" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Order Date</label>
											<input type="text" class="form-control"name="order_date" value="{{$order->order_date}}" id="nama_category" placeholder="Masukkan Harga" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Payment Due</label>
											<input type="text" class="form-control"name="payment_due" value="{{$order->payment_due}}" id="nama_category" placeholder="Masukkan Deskripsi" required>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
												<div class="col-md-12">
											<label for="validationCustom01" class="form-label">Payment Status</label>

<select class="form-control" name="payment_status" id="combo1">
	<option value="">Pilih</option>
	<option value="PENDING">PENDING</option>
	<option value="SUCCESS">SUCCESS</option>
		<option value="CANCEL">CANCEL</option>

</select>
											<div class="valid-feedback">Looks good!</div>
										
										</div>
										<div class="col-12">
											<button class="btn btn-light" type="submit">Submit</button>
										</div>
									</form>
								</div>
							</div>
						</div>
				<!--end row-->
				<br>
				<br>
					<br>
				<br>

			</div>


@endsection