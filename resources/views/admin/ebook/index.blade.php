@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
						<h6 class="mb-0 text-uppercase">Ebook</h6>
				<hr/>
		
			 <div class="col-12">
                <a href="ebook/create" class="btn btn-primary">Tambah</a>
            </div>
            <br>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">  <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Id Kategori</th>
                <th scope="col">User Id</th>
                <th scope="col">Nama</th>
                <th scope="col">File PDF</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Harga</th>   
               <th scope="col" width="250">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($ebook as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->user_id}}</td>
                        <td>{{$value->category_id}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->file_pdf}}</td>
                        <td>{{$value->deskripsi}}</td>
                           <td>{{$value->harga}}</td>
                        <td>
                            <a href="/ebook/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/ebook/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                              <a href="/ebook/{{$value->id}}/delete" class="btn btn-danger">Delete</a>
                               
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
               <tfoot class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Id Kategori</th>
                <th scope="col">User Id</th>
                <th scope="col">Nama</th>
                <th scope="col">File PDF</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Harga</th>   
               <th scope="col" width="250">Actions</th>
              </tr>
            </tfoot>
        </table>
						</div>
					</div>
				</div>

			</div>


@endsection