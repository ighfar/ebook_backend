<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Order;
use pdf;
use Illuminate\Support\Facades\Validator;
use UxWeb\SweetAlert\SweetAlert;
use Maatwebsite\Excel\Facades\Excel;

class TransaksiController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $order = Order::all();
        return view('admin.transaksi.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $ebook = Ebook::find($id);
        return view('ebook.show', compact('ebook'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
           $order = Order::find($id);
        return view('admin.transaksi.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
            'user_id' => 'required | numeric',
               'code'       => 'required',
            'status'       => 'required',
            'grand_total'    => 'required',
            'order_date' => 'required',
            'payment_due' => 'required',
            'payment_status'       => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
   

        $order = Order::find($id);
        $order->user_id = $request->user_id;
              $order->code = $request->code;
        $order->status = $request->status;
        $order->grand_total = $request->grand_total;
        $order->order_date = $request->order_date;
        $order->payment_due = $request->payment_due;
               $order->payment_status = $request->payment_status;
        $order->update();
              //  SweetAlert::success('Success Message','Data berhasil diedit');
        return redirect('/transaksi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {      
        $transaksi = Order::find($id);
        $transaksi->delete();
              //  SweetAlert::success('Success Message','Data berhasil dihapus');
        return redirect('/transaksi');
    }
       
}
