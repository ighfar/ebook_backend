<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Ebook;

class EbookController extends Controller
{
    public function index() {
        return Ebook::all();
    }

   

    public function detail_ebook($id)
    {
        $ebook = Ebook::whereId($id)->first();


        if ($ebook) {
            return response()->json([
                'success' => true,
                'message' => 'Detail Ebook!',
                'data'    => $ebook
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Ebook Tidak Ditemukan!',
                'data'    => ''
            ], 401);
        }
    }
    public function create(Request $request){
            $userid = Auth::user()->id;
        $validator = Validator::make($request->all(), [
            'category_id' => 'required | numeric',
            'name'       => 'required',
            'file_pdf' => 'required|file|mimes:pdf|max:2048',
            'harga' => 'required | numeric',
            'deskripsi'       => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $file = $request->file('file_pdf')->getClientOriginalName();
        $request->file('file_pdf')->move('upload',$file);
        $data = [
            'category_id' => $request->input('category_id'),
            'user_id'  => $userid,
            'name' => $request->input('name'),
            'file_pdf' => $file,
            'harga' => $request->input('harga'),
            'deskripsi' => $request->input('deskripsi'),
        ];
        $ebook = Ebook::create($data);
        return response()->json('Data berhasil disimpan ');
 
    }
    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'category_id' => 'required | numeric',
            'name'       => 'required',
            'file_pdf' => 'required|file|mimes:pdf|max:2048',
            'harga' => 'required | numeric',
            'deskripsi'       => 'required',
        ]);
        
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $ebook = Ebook::find($id);
        $file = $request->file('file_pdf')->getClientOriginalName();
        $request->file('file_pdf')->move('upload',$file);
        $data = [
            'category_id' => $request->input('category_id'),
            'name' => $request->input('name'),
            'file_pdf' => $file,
            'harga' => $request->input('harga'),
            'deskripsi' => $request->input('deskripsi'),
        ];
        $ebook = Ebook::update($data);
        return response()->json('Data berhasil diupdate ');
 
    }
            public function ebookbycategory($category){

              $ebooks = Ebook::where('category_id', '=', $category)->get()->toArray();
        return response()->json($ebooks, 201);
 
    }
        public function delete($id){

            $ebook = Ebook::find($id);
            $ebook->delete();
     
        return response()->json('Data berhasil dihapus ');
 
    }
}