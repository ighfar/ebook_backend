@extends('layouts.admin')
@section('main-content')


			<div class="page-content">
				<!--breadcrumb-->

				<!--end breadcrumb-->
				<br>
						<h6 class="mb-0 text-uppercase">Transaksi</h6>
				<hr/>
		
			<br>
				<div class="card">
					<div class="card-body">
						<div class="table-responsive">  <<table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">User Id</th>
                <th scope="col">Code</th>
                <th scope="col">Status</th>
                <th scope="col">Total</th>
                <th scope="col">Order Date</th>
                <th scope="col">Payment Due</th>   
                   <th scope="col">Payment Status</th>   
               <th scope="col" width="250">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($order as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->user_id}}</td>
                        <td>{{$value->code}}</td>
                        <td>{{$value->status}}</td>
                        <td>{{$value->grand_total}}</td>
                        <td>{{$value->order_date}}</td>
                           <td>{{$value->payment_due}}</td>

                           <td>{{$value->payment_status}}</td>
                        <td>
         
                            <a href="/transaksi/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                               <a href="/transaksi/{{$value->id}}/delete" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
                  <tfoot class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">User Id</th>
                <th scope="col">Code</th>
                <th scope="col">Status</th>
                <th scope="col">Total</th>
                <th scope="col">Order Date</th>
                <th scope="col">Payment Due</th>   
                   <th scope="col">Payment Status</th>   
               <th scope="col" width="250">Actions</th>
              </tr>
            </tfoot>
        </table>
						</div>
					</div>
				</div>

			</div>


@endsection