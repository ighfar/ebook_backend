<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Ebook;
use Validator;
use Auth;

class UserController extends Controller
{
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string','email'],
            'password' => ['required','string'],
        ]);
        if ($validator->fails()){
            return response()->json('Login Failed',422,$validator->error());
        }
        if (Auth::attempt(['email'=> $request->email, 'password'=>$request->password]))
        {
            $user = Auth::user();
            $response = [
                'token' => $user->createToken('MyToken')->accessToken,
                'name' => $user->name,
            ];
            return response()->json($response);
        }else{
            return response()->json('Wrong email or password',401);
        }

    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',

            'email' => 'required|string|email|max:255|unique:users',
              'password' => 'required|string|min:6',
              
           
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $params = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            
            'password' => Hash::make($request->get('password')),
       
        ];
        if ($user = User::create($params)){
            $token = $user->createToken('MyToken')->accessToken;
            $response = [
                'token' => $token,
                'user' => $user,
            ];
            return response()->json($response);
        }else{
            return response()->json('registrasi gagal',401);
        
        }

} 
public function profil(Request $request)
{
    return response()->json($request->user());
    
    }
   public function update(Request $request, $id){
     
   
        $doc = User::find($id);

         $doc->update($request->all());
        

        return $doc;
        
    }
      public function ebookku(Request $request){
           $userid = Auth::user()->id;

              $ebooks = Ebook::where('user_id', '=', $userid)->get()->toArray();
        return response()->json($ebooks, 201);
 
    }
public function change_password(Request $request)
{
    $input = $request->all();
    $userid = Auth::user()->id;
    $rules = array(
        'old_password' => 'required',
        'new_password' => 'required|min:6',
        'confirm_password' => 'required|same:new_password',
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {
        try {
            if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                $arr = array("status" => 400, "message" => "Check your old password.", "data" => array());
            } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                $arr = array("status" => 400, "message" => "Please enter a password which is not similar then current password.", "data" => array());
            } else {
                User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                $arr = array("status" => 200, "message" => "Password updated successfully.", "data" => array());
            }
        } catch (\Exception $ex) {
            if (isset($ex->errorInfo[2])) {
                $msg = $ex->errorInfo[2];
            } else {
                $msg = $ex->getMessage();
            }
            $arr = array("status" => 400, "message" => $msg, "data" => array());
        }
    }
    return \Response::json($arr);
}   

        public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'logout sukses',
        ]);
    }
}